﻿using System.Threading.Tasks;

namespace ThermonuclearWarApiWrapper
{
    public interface IThermonuclearWar
    {
        Task<bool> IsOnline();

        Task<LaunchResponse> Launch(string launchCode);
    }
}