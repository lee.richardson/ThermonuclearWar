﻿namespace ThermonuclearWarApiWrapper
{
    public enum OnlineStatus
    {
        Online,
        Offline
    }
}