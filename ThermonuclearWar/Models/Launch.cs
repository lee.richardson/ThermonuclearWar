﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ThermonuclearWar.Models
{
    public class Launch
    {
        public Launch()
        {
        }

        public Launch(DateTime dateTime, string launchCode)
        {
            DateTime = dateTime;
            LaunchCode = launchCode;
        }

        [Key]
        public int Id { get; set; }

        [DisplayName("DateTime")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DateTime { get; set; }

        [DisplayName("Launch Code")]
        public string LaunchCode { get; set; }
    }
}