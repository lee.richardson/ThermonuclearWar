﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ThermonuclearWar.Models
{
    public class ThermonuclearWarDbContext : DbContext
    {
        public ThermonuclearWarDbContext() : base("ThermonuclearWarDbContext")
        {
        }

        public virtual DbSet<Launch> Launches { get; set; }
    }
}