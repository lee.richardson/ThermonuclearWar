# Installation
> `npm install --save @types/angular`

# Summary
This package contains type definitions for Angular JS (http://angularjs.org).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/angular

Additional Details
 * Last updated: Tue, 21 Mar 2017 20:18:42 GMT
 * Dependencies: jquery
 * Global values: angular, ng

# Credits
These definitions were written by Diego Vilar <http://github.com/diegovilar>, Georgii Dolzhykov <http://github.com/thorn0>.
