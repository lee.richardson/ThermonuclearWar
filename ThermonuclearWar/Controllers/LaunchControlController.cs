﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Results;
using ThermonuclearWar.Models;
using ThermonuclearWar.Services;
using ThermonuclearWarApiWrapper;

namespace ThermonuclearWar.Controllers
{
    [RoutePrefix("api/LaunchControl")]
    public class LaunchControlController : ApiController
    {
        public LaunchControlService LaunchControlService { get; }

        public LaunchControlController()
        {
            this.LaunchControlService = new LaunchControlService();
        }
        
        public LaunchControlController(LaunchControlService launchControlService)
        {
            this.LaunchControlService = launchControlService;
        }

        // GET: api/LaunchControl/IsOnline
        [Route("IsOnline")]
        [HttpGet]
        [ResponseType(typeof(bool))]
        public async Task<IHttpActionResult> IsOnlineAsync()
        {
            var wrapper = new ThermonuclearWarApiWrapper.ThermonuclearWar();
            var status = await wrapper.IsOnline();
            return Json(status);
        }

        // GET: api/LaunchControl/Launch
        [Route("Launch")]
        [HttpPost]
        [ResponseType(typeof(LaunchResponse))]
        public async Task<IHttpActionResult> LaunchAsync()
        {
            try
            {
                if (!this.LaunchControlService.IsLaunchCooldownPassed())
                {
                    var lockedResponse = this.LaunchControlService.GetFailedCooldownResponse();
                    return lockedResponse;
                }

                var launchResponse = await LaunchControlService.LaunchAsync();

                if (launchResponse.Result == "Success")
                {
                    return Json(launchResponse);
                }

                var responseMessage = this.LaunchControlService.GetAppropriateErrorResponseMessage(launchResponse);
                return responseMessage;
            }
            catch (Exception exception)
            {
                // So we always return a LaunchResponse
                // TODO: Logging.

                var launchResponse = new LaunchResponse()
                {
                    Result = "Failure",
                    Message = "An error occured. The responsible persons have been thoroughly shot.",
                };

                var responseMessageResult = LaunchControlService.BuildResponseMessage(HttpStatusCode.InternalServerError, launchResponse);
                return responseMessageResult;
            }
        }
    }
}